" Vim configuration files maintained by Jochen Schroeder <cycomanic@gmail.com>.
" Feel free to use modify or do whatever you want with it.
" 
" ================================================================
" A list of useful urls with configuration tips:
"
" http://nvie.com/posts/how-i-boosted-my-vim/
"
"==================================================================
" begin .vimrc

" General options                                                           {{{
set nocompatible                "use in vim mode not vi compatibility 
"set nofs                       "do not flush to disk uncomment on laptops to save battery
set mouse=a                     " enable using the mouse if terminal emulator
                                "    supports it (xterm does)
set mousemodel=extend           " right mouse button extends selection


" Use pathogen to easily modify the runtime path to include all plugins under
" the ~/.vim/bundle directory
" call pathogen#runtime_append_all_bundles()
call pathogen#infect()
call pathogen#helptags()
" }}}

" Editing option {{{
filetype off                    " force reload of filetype for pathogen
filetype on                     " set filetype stuff to on
filetype plugin on
filetype indent on
" }}}

" Wrapping options                                                          {{{
set lbr!                        " wrap at words 
"set nowrap                     " don't wrap lines
set tabstop=4                   " a tab is four spaces
set backspace=indent,eol,start  " allow backspacing over everything in insert mode
set softtabstop=4               " see 4 spaces as a tab, when moving backwards"
set shiftwidth=4                " number of spaces to use for autoindenting
set shiftround                  " use multiple of shiftwidth when indenting with '<' and '>'
set showmatch                   " set show matching parenthesis
set expandtab                   " expand tabs to whitespace"
set textwidth=79                " set the textwidth to a proper terminal width
set smarttab                    " insert tabs on the start of a line according to
                                "    shiftwidth, not tabstop
"set formatoptions=tcqa         " automatic line formating according to textwidth
" }}}
                                
" Folding options                                                           {{{
set foldenable                  " enable folding
set foldcolumn=2                " add a fold column
set foldmethod=marker           " detect triple-{ style fold markers
set foldopen=block,hor,insert,jump,mark,percent,quickfix,search,tag,undo
                                " which commands trigger auto-unfold
" }}}

" Search options                                                            {{{
set hlsearch                    " highlight search terms
set incsearch                   " show search matches as you type
set ignorecase                  " ignore case when searching
set smartcase                   " ignore case if search pattern is all lowercase,
                                "    case-sensitive otherwise
" }}}

" Intend options                                                            {{{
set autoindent                  " always set autoindenting on
set copyindent                  " copy the previous indentation on autoindenting
set scrolloff=4                 " keep 4 lines off the edges of the screen when scrolling
set virtualedit=all             " allow the cursor to go in to "invalid" places
set nolist                      " don't show invisible characters by default
set listchars=tab:»·,trail:·,extends:#,nbsp:·
set pastetoggle=<F2>            " when in insert mode, press <F2> to go to
                                "    paste mode, where you can paste mass data
                                "    that won't be autoindented
" }}}

" General editor behaviour                                                  {{{
set termencoding=utf-8
set encoding=utf-8
set lazyredraw                  " don't update the display while executing macros
set laststatus=2                " tell VIM to always put a status line in, even
                                "    if there is only one window
set cmdheight=1                 " use a status bar that is 1 rows high
set number                      " always show line numbers
set title                       " change the terminal's title
set visualbell                  " don't beep
set noerrorbells                " don't beep
set showcmd                     " show (partial) command in the last line of the screen
                                "    this also shows visual selection info
set modeline                    " allow files to include a 'mode line', to
                                "    override vim defaults
set modelines=5                 " check the first 5 lines for a modeline
set ruler                       " show position in file
set spell spelllang=en_au
" }}}

" Vim behaviour                                                             {{{
set hidden                      " hide buffers instead of closing them this
                                "    means that the current buffer can be put
                                "    to background without being written; and
                                "    that marks and undo history are preserved
set switchbuf=useopen           " reveal already opened files from the
                                " quickfix window instead of opening new
                                " buffers
set history=1000                " remember more commands and search history
set undolevels=1000             " use many muchos levels of undo
set nobackup                    " do not keep backup files, it's 70's style cluttering
set directory=~/.vim/swap
                                " store swap files in one of these directories
set viminfo='1000,f1,<400        " read/write a .viminfo file for a thousand files max
                                "    save global marks and a maximum of 400
                                "    lines
set wildmenu                    " make tab completion for files/buffers act like bash
set wildmode=list:full          " show a list when pressing tab and complete
                                "    first full match
set wildignore+=*.pyc,*.zip,*.gz,*.bz,*.tar,*.jpg,*.png,*.gif,*.avi,*.wmv,*.ogg,*.mp3,*.mov
syntax on                       " turn syntax highlighting on
set pumheight=4                 " set a maximum height for popup windows

" }}}

" Gui dependent options                                                     {{{
set guifont=DejaVu\ Sans\ Mono\ 11 
if has("gui_running")
    colorscheme jellybeans
    set columns=90              " make gvim 90 columns wide
    set lines=100
else
    let xterm16bg_Normal = 'none'
    let xterm16_colormap = 'soft'
    let xterm16_brightness = 'high'
    colorscheme xterm16
endif
let &guicursor = &guicursor . ",a:blinkon0"
                                "turn off blinking cursor in gvim
" }}}

" Latex-suite:                                                              {{{
let g:tex_flavor="latex"        " REQUIRED. This makes vim invoke latex-suite 
                                "  when you open a tex file.

set grepprg=grep\ -nH\ $*       " IMPORTANT: grep will sometimes skip 
                                "  displaying the file name if you
                                "  search in a singe file. This will confuse 
                                "  latex-suite. Set your grep program to 
                                "  always generate a file-name.
" }}}

" AutomaticTex                                                          {{{
let b:atp_TexCompiler = "pdflatex"
let g:atp_ProgressBar = 1
let b:atp_ProjectScript = 0
" }}}
 
" Keybindings:                                                              {{{
" Since I never use the ; key anyway, this is a real optimization for almost
" all Vim commands, since we don't have to press that annoying Shift key that
" slows the commands down
nnoremap ; :

" Use Q for formatting the current paragraph (or visual selection)
vmap Q gq
nmap Q gqap

" make p in Visual mode replace the selected text with the yank register
vnoremap p <Esc>:let current_reg = @"<CR>gvdi<C-R>=current_reg<CR><Esc>

nmap mk :make<CR>

" Swap implementations of ` and ' jump to markers
" By default, ' jumps to the marked line, ` jumps to the marked line and
" column, so swap them
nnoremap ' `
nnoremap ` '

" Change the mapleader from \ to , let mapleader=","

" Use the damn hjkl keys
" map <up> <nop>
" map <down> <nop>
" map <left> <nop>
" map <right> <nop>

" Remap j and k to act as expected when used on long, wrapped, lines
nnoremap j gj
nnoremap k gk

" Easy window navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" Complete whole filenames/lines with a quicker shortcut key in insert mode
imap <C-f> <C-x><C-f>
imap <C-l> <C-x><C-l>

" Use ,d (or ,dd or ,dj or 20,dd) to delete a line without adding it to the
" yanked stack (also, in visual mode)
nmap <silent> <leader>d "_d
vmap <silent> <leader>d "_d

" Quick yanking to the end of the line
nmap Y y$

" Yank/paste to the OS clipboard with ,y and ,p
nmap <leader>y "+y
nmap <leader>Y "+yy
nmap <leader>p "+p
nmap <leader>P "+P
nmap <leader>r :registers<CR>

" Edit the vimrc file
nmap <silent> <leader>ev :e $MYVIMRC<CR>
nmap <silent> <leader>sv :so $MYVIMRC<CR>

" Clears the search register
nmap <silent> <leader>/ :nohlsearch<CR>

" Quickly get out of insert mode without your fingers having to leave the
" home row (either use 'jj' or 'jk')
inoremap jj <Esc>
inoremap jk <Esc>

" Quick alignment of text
nmap <leader>al :left<CR>
nmap <leader>ar :right<CR>
nmap <leader>ac :center<CR>

" Sudo to write
cmap w!! w !sudo tee % >/dev/null

" Pull word under cursor into LHS of a substitute
nmap <leader>z :%s#\<<C-r>=expand("<cword>")<CR>\>#

"moving around in tabs using ALT-left ALT-right
map <silent><A-Right> :tabnext<CR>
map <silent><A-Left> :tabprevious<CR>

" move between tags with CTRL-left CTRL-right
map <silent><C-Left> <C-T>
map <silent><C-Right> <C-]>

" toggle between single and double with window
map <M-[> :set columns=180<CR>  
map <M-]> :set columns=90<CR>  

" Gundo toggle
map <leader>g :GundoToggle<CR>

" maps for using the rope plugin
map <leader>j :RopeGotoDefinition<CR>
map <leader>r :RopeRename<CR>

" fuzzy searching with ack plugin
nmap <leader>a <Esc>:Ack!

" Syntastic open Error list
map <leader>E :Errors<CR>

" go to next item in location list
nmap <C-j> :1lnext<CR>
nmap <C-k> :1lprevious<CR>
" }}}

" Managing buffers with LustyJuggler                                        {{{
map ,b :LustyJuggler<CR>
" }}}

" TagList settings                                                          {{{
nmap <leader>l :TlistClose<CR>:TlistToggle<CR>
nmap <leader>L :TlistClose<CR>

" quit Vim when the TagList window is the last open window
let Tlist_Exit_OnlyWindow=1         " quit when TagList is the last open window
let Tlist_GainFocus_On_ToggleOpen=1 " put focus on the TagList window when it opens
"let Tlist_Process_File_Always=1     " process files in the background, even when the TagList window isn't open
"let Tlist_Show_One_File=1           " only show tags from the current buffer, not all open buffers
let Tlist_WinWidth=40               " set the width
let Tlist_Inc_Winwidth=1            " increase window by 1 when growing

" shorten the time it takes to highlight the current tag (default is 4 secs)
" note that this setting influences Vim's behaviour when saving swap files,
" but we have already turned off swap files (earlier)
"set updatetime=1000

" the default ctags in /usr/bin on the Mac is GNU ctags, so change it to the
" exuberant ctags version in /usr/local/bin
let Tlist_Ctags_Cmd = '/usr/local/bin/ctags'

" show function/method prototypes in the list
let Tlist_Display_Prototype=1

" don't show scope info
let Tlist_Display_Tag_Scope=0

" show TagList window on the right
let Tlist_Use_Right_Window=1

" }}}

" Filetype specific handling                                                {{{
" only do this part when compiled with support for autocommands
if has("autocmd")

   " highlight any line longer than 80 chars
   autocmd filetype python,rst match ErrorMsg '\%>79v.\+'

   " highlight spaces in python
   autocmd filetype python,rst set list
   autocmd filetype python,rst set listchars=tab:»·,trail:·,extends:#
   
   " Python runners
   autocmd filetype python map <F5> :w<CR>:!python %<CR>
   autocmd filetype python imap <F5> <Esc>:w<CR>:!python %<CR>
   autocmd filetype python map <S-F5> :w<CR>:!ipython %<CR>
   autocmd filetype python imap <S-F5> <Esc>:w<CR>:!ipython %<CR>

   " use closetag plugin to auto-close HTML tags
   autocmd filetype html,xml,xsl source ~/.vim/scripts/html_autoclosetag.vim

endif " has("autocmd")

" Python omnicomplete
au FileType python set omnifunc=pythoncomplete#Complete
" }}}

" Syntastic Syntax checker options {{{

" Statusline 
    set statusline+=%#warningmsg#
    set statusline+=%{SyntasticStatuslineFlag()}
    set statusline+=%*

" Error symbols
    let g:syntastic_error_symbol='✗'
    let g:syntastic_warning_symbol='⚠'

" Other
    let g:syntastic_check_on_open=1 "syntax check files when opened
  "  let g:syntastic_always_populate_loc_list=1
  "  let g:syntastic_auto_loc_list=1
"}}}

" Supertab plugin                                                       {{{
   let g:SuperTabDefaultCompletionType = "context"     " enable supertab in context
"let g:SuperTabMappingForward = '<c-space>'          " circle back and forth 
                                                    " between mappings with 
                                                    " ctrl space and ctrl shift
                                                    " space
"let g:SuperTabmappingbackward = '<s-c-space>' 
"set completeopt=menuone,longest,preview             " options that show pydoc previews

" }}}

" Auto save/restore {{{
"au BufWritePost *.* silent mkview!
"au BufReadPost *.* silent loadview
" This new option does not give an error with pyflakes
autocmd BufWinLeave ?* mkview
"autocmd BufWinEnter ?* silent loadview


" Quick write session with F2
map <F2> :mksession! .vim_session<CR>
" And load session with F3
map <F3> :source .vim_session<CR>
" }}}

" List of plugins I use                                                     {{{
" latex-suite, supertab, surround, closetag, python.vim, vimball, command-t,
" tcomment, snipMate, pyflakes
" }}}
